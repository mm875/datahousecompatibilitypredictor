This solution takes input in the form of a JSON data file. The 
compatibility_predictor.py file has a function that reads in the JSON file 
and loads it into the Python dictionary format. The other input that this 
solution takes is user input in the form of attribute ranking specification. 
Upon running the solution, the user will be prompted to rank the attributes 
in terms of importance, based upon those provided in the JSON data file.

After processing the JSON file and determining the attribute rankings, the 
solution compiles the attributes of each member into an array. There is then 
a for-loop that iterates through each applicant, calling upon a function 
that determines an incompatibility score with the use of three parameters: the 
applicant's attributes, the members' attributes, and the attribute rankings. 
This function, get_incompatibility_score, iterates through each member and 
compares their attributes to the specific applicant. For each applicant, their 
attribute differences with each member will be added together and then divided 
by the number of team members, resulting in an average difference for each 
attribute for each applicant in the form of a dictionary. This function then 
scales the average differences based upon the attribute rankings. These 
average differences are then passed to a get_standard_deviation function, 
which takes the square root of the sum of the squared differences to produce 
an incompatibility score. This value is returned to main where it is 
properly formatted. Additionally, given that the solution seeks to produce a 
compatibility score, each incompatibility score is passed to the 
get_normalized_compatibility function. This function assumes that the 
minimum incompatibility would be 0 and that the maximum would be 10, and 
based upon this, the compatibility scores can be normalized. After the 
compatibility scores have been finalized and properly formatted as a 
dictionary, the main writes to a new JSON file that displays the solution.
