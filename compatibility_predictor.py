import json
import math

def json_to_dict(json_file):
    with open(json_file, "r") as file:
        data = json.load(file)
    return data

def get_standard_deviation(scaled_avg_differences):
    squared_differences_sum = 0
    for difference in scaled_avg_differences.values():
        squared_differences_sum += difference * difference
    return math.sqrt(squared_differences_sum)

def get_incompatibility_score(applicant_attributes, member_attributes,
                            attribute_importance):
    avg_differences = {}
    for member in member_attributes:
        attribute_difference = {}
        for attribute in applicant_attributes.keys():
            attribute_difference[attribute] = applicant_attributes[attribute]\
                                              - member[attribute]
        for attribute in attribute_difference:
            if attribute not in avg_differences:
                avg_differences[attribute] = 0
            avg_differences[attribute] += attribute_difference[attribute]
    avg_differences.update((k, v / len(member_attributes)) for k, v in
                           avg_differences.items())
    for difference in avg_differences:
        avg_differences[difference] /= float(attribute_importance[difference])
    scaled_avg_differences = avg_differences
    incompatibility_score = get_standard_deviation(scaled_avg_differences)
    return incompatibility_score

def get_attribute_importance(data):
    num_attributes = len(data["applicants"][0]["attributes"].keys())
    print("There are " + str(num_attributes) + " attributes. Rank them "
                                               "according to importance (1 for "
                                               "the most important and " +
          str(num_attributes) + " for the least important):")
    attribute_importance = {}
    for attribute in data["applicants"][0]["attributes"].keys():
        rank = input("Enter the rank of " + attribute + ": ")
        attribute_importance[attribute] = rank
    return attribute_importance

def get_normalized_compatibility(applicant_scores):
    for applicant_score in applicant_scores:
        normalized_score = 1 - (applicant_score["score"] / 10)
        applicant_score["score"] = round(normalized_score, 2)
    return applicant_scores

if __name__ == "__main__":
    json_file = "sample_data.json"
    data = json_to_dict(json_file)
    attribute_importance = get_attribute_importance(data)
    member_attributes = []
    for member in data["team"]:
        member_attributes.append(member["attributes"])
    applicant_scores = []
    for applicant in data["applicants"]:
        incompatibility_score = get_incompatibility_score\
            (applicant["attributes"], member_attributes, attribute_importance)
        scored_applicant = {}
        scored_applicant["name"] = applicant["name"]
        scored_applicant["score"] = incompatibility_score
        applicant_scores.append(scored_applicant)
    applicant_scores = get_normalized_compatibility(applicant_scores)
    final_dict = {"scoredApplicants": applicant_scores}
    file_path = "compatibility_scores.json"
    with open(file_path, "w") as json_file:
        json.dump(final_dict, json_file)

